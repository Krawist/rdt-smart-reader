package cm.seeds.rdtsmartreader.data
data class DhisResponse(
        val httpStatus : String = "",
        val httpStatusCode : Int = 0,
        val status : String = "",
        val message : String = ""
)