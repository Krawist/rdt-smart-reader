package cm.seeds.rdtsmartreader.modeles

class SynchronisationParams (

     var program : String = "Euo95Y8QMfD",
     var orgUnit : String = "VvF1bY8mDyg",
     var eventDate : String = "",
     var status : String = "",
     var storedBy : String = "",
     var coordinate : Coordonnee,
     var dataValues : MutableList<DataValue>

)