package cm.seeds.rdtsmartreader.modeles

import cm.seeds.rdtsmartreader.helper.ServerListener

data class ServerState(
    var state : ServerListener.State
)
