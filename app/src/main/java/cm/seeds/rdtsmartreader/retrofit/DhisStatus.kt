package cm.seeds.rdtsmartreader.retrofit

enum class DhisStatus {
    SKIPPED,ACTIVE,SCHEDULE,VISITED,COMPLETED,OVERDUE
}