package cm.seeds.rdtsmartreader.modeles

data class Coordonnee(
        val latitude : Float,
        val longitude : Float,
)