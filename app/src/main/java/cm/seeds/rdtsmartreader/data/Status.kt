package cm.seeds.rdtsmartreader.data

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}